const express = require( 'express' );
const nodemailer = require ( 'nodemailer' );
const jwt = require ( 'njwt' );
const { body, validationResult } = require( 'express-validator' );
const { MongoClient, ObjectId } = require( 'mongodb' );

const router = express.Router();

const conString =
    'mongodb://localhost:27017/?readPreference=primary' + 
    '&appname=MongoDB%20Compass&directConnection=true&ssl=false';
const client = new MongoClient( conString );

function getCollection( dbName, collectionName ) {
    client.connect();
    const db = client.db( dbName );
    return db.collection( collectionName );
}

const secretKey = 'userApisecretkey7';

const registerValidation = [ 
    body( 'email', 'Invalid email' ).notEmpty().isEmail(),
    body( 'password', 'Password Should be more than 8 characters' )
    .notEmpty().isLength( { min: 8 } ),
    body( 'firstname', 'Only Alphabets Allowed' ).custom( ( value ) =>{
        if ( /^\D+$/.test( value ) ) {
            return true;
        } else {
            return false;
        }
    } ),
    body( 'lastname', 'Only Alphabets Allowed' ).custom( ( value ) =>{
        if ( /^\D+$/.test( value ) ) {
            return true;
        } else {
            return false;
        }
    } ),
];

const loginValidation = registerValidation.slice( 0, 2 );

const mailService = nodemailer.createTransport( {
    service: "Gmail",
    auth: {
        user: "psindustriesofficial@gmail.com",
        pass: "PKindustries37$"
    },
} );

const verifyLink = 'http://localhost:7777/user/verify';

router.use( express.json() );

router.post( '/', registerValidation, ( req, res ) => {
    const validation_Result = validationResult( req );
    if ( ! validation_Result.isEmpty() ) {
        res.status( 400 );
        res.json( { message: 'Bad Request' } );
    } else {
        const SampleTable = getCollection( 'demo', 'users' );

        SampleTable.insertOne( {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            verified: false
        } )
        .then( ( results ) => {
            const link = verifyLink + `/${ results.insertedId.toString() }`;
            const mailOptions = {
                from: 'psindustriesofficial@gmail.com',
                to: req.body.email,
                subject: 'Email Verification',
                text: `Please click the link ${ link } to verify your account `
            };
            mailService.sendMail( mailOptions, ( err, result ) => {
                if ( err ) {
                    res.status( 400 );
                    res.json( { message: 'Something went wrong!' } );
                } else {
                    res.send( 'User Registered Successfully!' );
                }
            } );
            client.close();
        } )
        .catch( ( err ) => {
            console.log( err );
            client.close();
            res.status( 400 );
            res.json( { message: 'Something went wrong!' } );
        } );

    }
} );

router.get( '/verify/:id', ( req, res ) => {
    const SampleTable = getCollection( 'demo', 'users' );

    SampleTable.updateOne(
        { _id: ObjectId( req.params.id ) },
        { $set : { verified: true } } )
    .then( ( results ) => {
        client.close();
        res.send( 'Your Account is Activated!' );
    } )
    .catch( ( err ) => {
        console.log( err );
        client.close();
        res.status( 400 );
        res.json( { message: 'Bad Request!' } );
    } );

} );

router.post( '/login', loginValidation, ( req, res ) => {
    const validation_Result = validationResult( req );
    if( ! validation_Result.isEmpty() ) {
        res.status( 400 );
        res.json( { message: 'Bad Request!' } );
        console.log( validation_Result );
    } else {
        const SampleTable = getCollection( 'demo', 'users' );
        SampleTable.findOne( { 
                email: req.body.email,
                password: req.body.password
            } )
        .then( ( result ) => {
            if ( result && result.verified ) {
                const data = {
                    id: result._id.toString(),
                };
                const token = jwt.create( data, secretKey );
                token.setExpiration( new Date().getTime() + 600 * 1000 )
                client.close();
                res.json( { message: 'Login Successfull!', token: token.compact() } );
            } else {
                res.status( 401 );
                client.close();
                res.json( {
                    message: 'unauthorized access',
                    data: 'Incorrect email or password'
                } );
            }
        } )
        .catch( ( err ) => {
            console.log( err );
            res.status( 400 );
            res.json( { message: 'Bad Request!' } );
        } );

    }
} );

router.get( '/:token', ( req, res ) => {
    const token = req.params.token;
    jwt.verify( token, secretKey, ( err, verifiedJwt ) => {
        if( err ) {
            res.status( 401 );
            res.json( { message: 'unauthorized access' } );
            console.log( err );
        } else {
            const SampleTable = getCollection( 'demo', 'users' );
            SampleTable.findOne( { _id: ObjectId( verifiedJwt.body.id ) } )
            .then( ( result ) => {
                const userToBeShowned = {
                    firstname: result.firstname,
                    lastname: result.lastname,
                    email: result.email
                }
                client.close();
                res.send( userToBeShowned );
            } )
            .catch( ( error ) => {
                res.status( 401 );
                client.close();
                res.json( { message: 'unauthorized access' } );
                console.log( error );
            } );

        }
    } );

} );

module.exports = router;