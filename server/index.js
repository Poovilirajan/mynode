const express = require( 'express' );
const products = require( './routes/products' );
const user = require( './routes/user' );
const port = 7777;

const customApi = express();

customApi.use( '/products', products );

customApi.use( '/user', user );

customApi.listen( port, () => {
    console.log( 'Api is running' );
} );